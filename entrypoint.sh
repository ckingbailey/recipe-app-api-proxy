#!/bin/sh

set -e # exit > 0 on fail

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;' # run nginx in the foreground on Docker
